import cobra
import efmtool
import h5py
import numpy as np
from cobra.util import create_stoichiometric_matrix

SUBSTRATE_EXCHANGES = [
    "EX_glc__D_e",
    "EX_gal_e",
    "EX_fru_e",
    "EX_ac_e",
    "EX_pyr_e",
    "EX_succ_e",
    "EX_glcn_e",
    "EX_glyc_e",
]

if __name__ == "__main__":
    # Load model and allow uptake of all substrates.
    model = cobra.io.read_sbml_model("ecoli-gerosa2015-data/data/e_coli_core_extended.xml")
    for id in SUBSTRATE_EXCHANGES:
        model.reactions.get_by_id(id).lower_bound = -10

    # Feed the model to efmtool and enumerate EFMs.
    S = create_stoichiometric_matrix(model)
    lb = np.array([r.lower_bound for r in model.reactions])
    ub = np.array([r.upper_bound for r in model.reactions])
    to_reverse = ub <= 0
    S[:, to_reverse] *= -1
    lb[to_reverse] *= -1
    is_reversible = [1 if b < 0 else 0 for b in lb]

    efms = efmtool.calculate_efms(
        S,
        is_reversible,
        [r.id for r in model.reactions],
        [m.id for m in model.metabolites],
    )
    efms[to_reverse, :] = -efms[to_reverse, :]
    print(f"Found {efms.shape} EFMS")

    # Store EFMs in a HDF5 file. Create datasets for float32 and float16 for usage on
    # memory-limited machines.
    reaction_ids = [r.id for r in model.reactions]
    with h5py.File("e_coli_core_extended_efms.h5", "w") as f:
        f.create_dataset("efms", data=efms.T)
        f.create_dataset("efms_float32", data=efms.T.astype("float32"))
        f.create_dataset("efms_float16", data=efms.T.astype("float16"))
        f.create_dataset("reaction_ids", data=reaction_ids)